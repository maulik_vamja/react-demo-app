import React, { useState } from "react";
import Form from "./components/Form";
import TodoList from "./components/List";
import "./App.css";

function App() {
  const [itemName, setItemName] = useState("");
  const [items, setItems] = useState([]);
  return (
    <div className="App">
      <header>
        <h1>To do List </h1>
      </header>
      <Form
        items={items}
        setItems={setItems}
        itemName={itemName}
        setItemName={setItemName}
      />
      <TodoList items={items} />
    </div>
  );
}

export default App;
