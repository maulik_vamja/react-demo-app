import React from "react";
import { FaCheck, FaTrash } from "react-icons/fa";

const Item = ({ itemName }) => {
  return (
    <div className="todo">
      <li className="todo-item">{itemName}</li>
      <button className="complete-btn">
        <FaCheck />
      </button>
      <button className="trash-btn">
        <FaTrash />
      </button>
    </div>
  );
};
export default Item;
