import React from "react";
import Item from "./Item";

const TodoList = ({ items }) => {
    return (
        <div className="todo-container">
            <ul className="todo-list">
                {items.map(item => (
                    <Item itemName={item.text} itemId={item.id} />
                ))}
            </ul>
        </div>
    );
}
export default TodoList;