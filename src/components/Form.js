import React from "react";
import { FaPlusSquare } from "react-icons/fa";

const Form = ({ itemName, setItemName, items, setItems }) => {
    const formSubmitHandler = (e) => {
        e.preventDefault();
        setItems([...items, { id: Math.random() * 100, text: itemName }]);
        setItemName('');
    };
    return (
        <form>
            <input value={itemName} onChange={(e) => setItemName(e.target.value)} type="text" placeholder="Enter your item" className="todo-input" />
            <button className="todo-button" type="submit" onClick={formSubmitHandler}>
                <FaPlusSquare />
            </button>
            {/* <div className="select">
                <select name="todos" className="filter-todo">
                    <option value="all">All</option>
                    <option value="completed">Completed</option>
                    <option value="uncompleted">Uncompleted</option>
                </select>
            </div> */}
        </form >
    );
}

export default Form;